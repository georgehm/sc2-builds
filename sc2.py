from tkinter import Tk
from tkinter.filedialog import askopenfilename, asksaveasfile, askopenfile
from sys import exit
import spawningtool.parser
import pickle

__author__ = "imp"
__credits__ = ["echo", "Jordan"]
# this script manages StarCraft 2 build orders.
# Allows you to extract build orders from replays or view saved ones from an .SC2Build file.

def sepWords(string, new_string=[]):  # turns 'FooBar' into 'Foo Bar'
	new_string = list(new_string)
	string = str(string)
	letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 
	'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	for x in range(0, len(string)):
		if len(string) < 2:
			new_string.append(string[x])
			break

		elif x != 0 and string[x] not in letters and string[x-1] is not " ":
			new_string.append(string.split(string[x::])[0])
			string = string[x::]
			return sepWords(string, new_string)

		elif x == len(string)-1:
			new_string.append(string)

	return " ".join(new_string)



def grabFile(fname, path=False, mute=False):
	assert type(fname) is str
	if not mute:
		print(f"Please select a .{fname} file.")
	Tk().withdraw()
	file = askopenfilename()
	try:
		assert type(file) is str
		assert f".{fname}" in file
	except AssertionError:
		exit(f"Invalid file! must be a .{fname} file.")

	if not path:
		file = open(file, "rb+")

	return file


# saves a file in one of two ways:
# if the arg data is passed: pickle dumps the data and saves the file
# if data isn't passed: create file and return the file to the caller, allowing them to do whatever
def saveFile(exten, data=None, mode="xb+", mute=False):
	assert type(exten) is str
	if not mute:
		print("Please save the file.")
	Tk().withdraw()
	file = asksaveasfile(mode=mode, defaultextension=f".{exten}")
	if file is None:
		return exit("Failed to save file.")
	elif data and "b" in mode:
		pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
		file.close()
		return True
	else:
		return file


# file selection
def grabBuild():
	data = spawningtool.parser.parse_replay(grabFile("SC2Replay", "path"))
	# we have a valid replay file

	assert data['buildOrderExtracted'] == True # make sure we got the build order

	user = 0 # user is 0 unless a proper selection is made
	while user != 1 and user != 2:  # pick player to get build from
		user = input(f"\nWhich player would you like to get a build from? "
					f"\n1) {data['players'][1]['name']}\n2) {data['players'][2]['name']}\n> ")
		try:
			user = int(user)
		except ValueError:
			user = 0

	build_data = data['players'][user]['buildOrder']
	# ^ get the players build

	# assemble the build order into a readable format, removing workers
	array = []
	for x in build_data:
		workers = ["SCV", "Probe", "Drone"]
		if x['name'] not in workers:
			line_ar = [str(x['supply']), str(x['time']), str(sepWords(x['name']))]
			array.append(" ".join(line_ar))

	print("\n".join(array))
	return array


option = input("Do you wish to:\n1) open a replay file\n2) open a build order\n> ")

# open a replay file
if "1" in option:
	# this whole section is pretty
	data = grabBuild()
	option = input("\n\nDo you wish to:\n1) save this build order to a new file\n2) add this build to an existing "
				"collection\n3) exit\n> ")
	if option in ['1', '2']:
		if "1" in option:
			option = input("Do you wish to save this as:\n1) .SC2Build file 2)\n.txt file"
				"\n\nNote:\n.SC2 files are only readable within this script, but allow multiple builds to be within one file."
				"\n.txt is a text file with the build order listed.\n> ")

			if "1" in option:
				title = input("Please title this build.\n> ")
				saveFile(data={title:data}, exten="SC2Build", mode="xb+")
				exit(f"Build saved successfully as '{title}'")
			elif "2" in option:
				data = {title:data}
				with saveFile(exten="txt" ,mode="xw+") as f:
					f.write("\n".join(build))
				exit("Build saved successfully.")
			else:
				exit()

		elif "2" in ans:
			with grabFile("SC2Build") as f:
				file_data = pickle.load(f)
				file_data[title] = data
				pickle.dump(file_data, open(f.name, "wb"), protocol=pickle.HIGHEST_PROTOCOL)
				print(f"'{title}' saved successfully.")

	else:
		exit()

# open a build order
elif "2" in option:
	with grabFile("SC2Build") as f:
		build = pickle.load(f)

	print("\nBuilds found:")
	num = 1
	build_index = {}
	for k, v in build.items():
		print(f"{num}) {k}")
		build_index[num] = k
		num+=1

	num = 0
	print("\nPlease select which build you would like to load")
	while num == 0:
		try:
			build_title = build_index[int(input("\n> "))]
			build = build[build_title]
			num = 1
		except (KeyError, ValueError):
			num = 0

	print("\n".join(build))
	option = input("\nOptions:\n1) Export to text file.\n2) Exit\n> ")
	if "1" in option:
		try:
			with open(f"{build_title}.txt", mode="x+") as f:
				f.write("\n".join(build))
			exit("Build exported successfully as '{}.txt'".format(build_title))
		except FileExistsError:
			exit("Error: file with title '{}' already exists.".format(build_title))

else:
	exit()